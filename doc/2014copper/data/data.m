close all
%set(0,'DefaultFigureWindowStyle','docked')
lw = 3.0; fz = 16;
set(0,'DefaultTextFontSize',fz)
set(0,'DefaultAxesFontSize',fz)
set(0,'defaultlinelinewidth',lw)
%
% plot convergence test - F-128, SR
%
load errors_F_128N;
load errors_F_128N_SR;
load resid_F_128N;
load resid_F_128N_SR;
[nr x] = size(resid_F_128N);
%load errors_F_128N_nonRCG;
[mf128 n] = size(errors_F_128N);
mf128 = mf128;
q2 = ones(mf128,1); q1 = q2;
N_F128 = 2*ones(mf128+1,1);
for i=1:mf128
  N_F128(i+1) = 2*N_F128(i);
  s_h = N_F128(i);
  q2(i) = 1/s_h^2;
  q1(i) = 1/s_h^1;
end
loglog(N_F128(mf128-nr+1:mf128),resid_F_128N,'go-.'),hold on % residual
loglog(N_F128(1:mf128),errors_F_128N_SR(:,1),'ms-.'),hold on % error
loglog(N_F128(mf128-nr+1:mf128),resid_F_128N_SR,'bd-.'),hold on % residual
loglog(N_F128(1:mf128),q2(1:mf128),'k:'),hold on
loglog(N_F128(1:mf128),q1(1:mf128)./4,'k-.'),hold on
set(gca,'XTick',N_F128(1:mf128),'Fontsize',fz)
legend('Residual','SR - Error','SR - Residual','Quadradic','Linear',3)
V=axis;
V(1)=N_F128(1);
V(2)=N_F128(mf128);
V(3)=errors_F_128N(mf128,1)/2;
V(4)=errors_F_128N(1,1)*2;
axis(V);
xlabel('N cells in X direction','Fontsize',fz);
ylabel('|.|_{infinity}','Fontsize',fz)
title(['1 F-cycle w/ V(2,2) cycles, Laplacian, u=(x^4 - R^2 x^2), ' ...
       '128^3 cells/core'],'Fontsize',fz)
%grid
set(findall(gcf,'type','text'),'FontSize',fz)
print('-djpeg100','converg_Fcycles_128N_edison')
print('-depsc2','converg_Fcycles_128N_edison')
print('-dpng','converg_Fcycles_128N_edison')
%
% finish off error plot
%
figure
load errors_V_128N;
[mv128 n] = size(errors_V_128N);
q2 = ones(mv128+1,1); q1 = q2;
N_V128 = 512*ones(mv128+1,1);
for i=1:mv128
  N_V128(i+1) = 2*N_V128(i);
  s_h = N_V128(i);
  q2(i) = 1/s_h^2*1.e-1;
end
loglog(N_F128(1:mf128),errors_F_128N(:,1),'bs-.'),hold on
loglog(N_F128(1:mf128),errors_F_128N_SR(:,1),'ms:'),hold on
loglog(N_V128(1:mv128),errors_V_128N(:,1),'rx--'),hold on
legend('1 F-cycle w/ V(2,2)','1 F-cycle w/ V(2,2) - SR','V(2,2) cycles, rtol=10.^{-4}',1),
set(gca,'XTick',N_F128(1:mf128))
V=axis;
V(1)=N_F128(1);
V(2)=N_F128(mf128);
V(3)=errors_F_128N(mf128,1)/2;
V(4)=errors_F_128N(2,1)*2;
axis(V);
xlabel('N cells in X direction');
ylabel('|error|_{infinity}')
title(['Error: Laplacian, u=(x^4 - R^2 ' ...
       'x^2), 128^3 cells/core'])
%grid
set(findall(gcf,'type','text'),'FontSize',fz)
print('-djpeg100','converg_errors_edison')
print('-depsc2','converg_errors_edison')
print('-dpng','converg_errors_edison')
%
% times
%
figure
fz2=14;
load times_F_128N_red
load times_F_128N_SR
load times_F_032N_red
load times_F_032N_SR
load times_F_032N_nonRCG
load times_V_128N
pes = 2*[8 64 512 4096 8*4096];
[m n] = size(times_F_128N_red);
semilogx(pes(1:m),times_F_128N_red,'r+-.'),hold on
semilogx(pes(1:m),times_F_128N_SR,'m*--'),hold on
semilogx(pes(1:m),times_V_128N,'g*--'),hold on
semilogx(pes(1:m),times_F_032N_red,'bo-.'),hold on
semilogx(pes(1:m),times_F_032N_nonRCG,'k*:'),hold on
semilogx(pes(1:m),times_F_032N_SR,'c+--'),hold on
V=axis;
V(1)=pes(1);
V(2)=pes(m);
V(3)=0;
V(4)=times_V_128N(m)*1.8;
%V(4)=times_F_032N_SR(m)*1.8;
axis(V);
set(gca,'XTick',pes(1:m))
xlabel('# cores (Edison)');
ylabel('Time')
title(['Solve times: 1xFMG w/ V(2,2), Laplacian, u=(x^4 - ' ...
       'R^2 x^2)'])
grid
set(findall(gcf,'type','text'),'FontSize',fz)
AX=legend('128^3 cells/core, 8 solves - non-redundant CGS','128^3 cells/core, 8 solves - SR, non-redundant CGS','V-cycles, 128^3 cells/core, rtol=10.^{-4}, 8 solves','32^3 cells/core, 512 solves - redundant CGS','32^3 cells/core, 512 solves - non-redundant CGS','32^3 cells/core, 512 solves - SR, non-redundant CGS',2);
LEG = findobj(AX,'type','text');
set(LEG,'FontSize',fz2);
print('-djpeg100','weak_scaling_edison')
print('-depsc2','weak_scaling_edison')
print('-dpng','weak_scaling_edison')
